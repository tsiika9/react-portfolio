import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Row, Col, Jumbotron } from 'reactstrap';

class Home extends Component {
    render() {
        return (
            <div>
                <Jumbotron fluid>
                        <h1 className="display-3">Tsiika.me</h1>
                        <p className="lead">This is a modified jumbotron that occupies the entire horizontal space of its parent.</p>
                        <p className="lead">You have stumbled upon portfolio of Tommi Siik. </p>
                        <p className="lead">Check more info at <NavLink to="/about">About</NavLink> page!</p>
                </Jumbotron>
                <div className="containeri">
                    <Row>
                        <Col className='ots'>
                            <h2>Home</h2>
                        </Col>
                    </Row>

                    <Row>
                        <Col md='6'>
                            <p>Welcome!</p>
                            <p>You have stumbled upon portfolio of Tommi Siik. </p>
                            <p>Check more info at <NavLink to="/about">About</NavLink> page!</p>
                        </Col>

                        <Col md='6'>
                        
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}

export default Home;