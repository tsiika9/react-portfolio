import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'devicon/devicon.min.css';
import 'devicon/devicon-colors.css';


ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
