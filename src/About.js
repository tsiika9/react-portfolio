import React, { Component } from 'react';
import { Container, Row, Col, Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle } from 'reactstrap';
import tsiika from'./img/tsiika.jpg';

class About extends Component {
    render() {
        return (
            <div>
                <Container>
                    <Row>
                        <Col className='ots'  md="6">
                            <h2>About</h2>
                        </Col>
                    </Row>
                    <Row>
                        <Col className='ots' md="6">
                            <h3>Who is this Tommi?</h3>
                            <p>I'm a self-taught web developer. Approximately 10 years I've been making website for my own amusement, but the calling to do websites for my profession has been lit lately. </p>
                            <p>In my pocket you can find skills to <b>HTML5</b>, <b>CSS3</b>, <b>Javascript</b> and <b>PHP</b>.</p>
                            <p>Familiar back-end tools for me is Node and Laravel. For front-end Bootstrap and Foundation are familiar tools. I've been working some experimental sites with <b>React</b> and <b>Vue</b>.</p>
                            <p>My spare time is dedicated to <a href="https://www.flickr.com/photos/tsiika/">photography</a>, playing with synthesizer and occationally piloting an spaceships.</p>
                        </Col>

                        <Col className='ots'  md="6">
                        <Card className="cardshadow">
                                <CardImg top width="100%" img  src={tsiika}  alt="Tommi Siik" />
                                    <CardBody className="cb">
                                        <CardTitle>Tommi Siik</CardTitle>
                                            <CardSubtitle>Card subtitle</CardSubtitle>
                                            <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                                    </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}

export default About;
