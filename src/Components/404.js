import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class Home extends Component {
    render() {
        return (
            <div>
                <h1 style={{ textShadow: 'black 5px 5px', fontSize: '60px'}}>404</h1>
                <h2>Seems like you are lost!</h2>
            </div>
        )
    }
}

export default Home;