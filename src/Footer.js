import React, { Component } from 'react';
import 'font-awesome/css/font-awesome.min.css';
import { Row, Col } from 'reactstrap';

class Footer extends Component {
    render() {
        return (
            <div>
                <Row>
                    <Col className='footer'>
                        <div class="container">
                            <div className="footer-block">
                                <h3>Contact</h3>
                                
                                <p alt="Email" className="lead" style={{ textDecoration: 'none', fontWeight: "900" }}>tsiika9@gmail.com</p> <br/><br/>
                                <i className="fa fa-linkedin"><a href="https://www.linkedin.com/in/tommi-siik-824473122/" alt="Linkedin"> LinkedIn </a></i>
                                <i className="fa fa-github"><a href="https://github.com/tsiika" alt="Github"> Github </a></i>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default Footer;
