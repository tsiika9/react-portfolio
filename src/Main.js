import React, { Component } from 'react';
import { Route, Link, BrowserRouter } from 'react-router-dom';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, Row, Col } from 'reactstrap';


import Home from "./Home";
import About from "./About";
import Works from "./Works";
import Blog from "./Blog";
import Footer from "./Footer";

class Main extends Component {
    constructor(props) {
        super(props);
    
        this.toggleNavbar = this.toggleNavbar.bind(this);
        this.state = {
            collapsed: true
        };
    }
    
        toggleNavbar() {
        this.setState({
            collapsed: !this.state.collapsed
        });
    }

    render() {
        return (
        <BrowserRouter>
            <div>

        <div>
        <Navbar expand="md" className="header navbar-light">
        <NavbarBrand href="/" className="mr-auto">Tsiika.me</NavbarBrand>
        <NavbarToggler onClick={this.toggleNavbar} className="mr-3"/>
        <Collapse isOpen={!this.state.collapsed} navbar>
            <Nav navbar>
            <NavItem>
            <NavLink tag={Link} exact to="/">Home</NavLink>
            </NavItem>

            <NavItem>
            <NavLink tag={Link} to="/about">About</NavLink>
            </NavItem>

            <NavItem>
            <NavLink tag={Link} to="/works">Works</NavLink>
            </NavItem>

            </Nav>
        </Collapse>
        </Navbar>

        
        </div>
                <div className="containeri">                
                    <Row>
                        <Col>
                            <div className="content">
                                    <Route exact path="/" component={Home}/>
                                    <Route path="/about" component={About}/>
                                    <Route path="/works" component={Works}/>
                                    <Route path="/blog" component={Blog}/>
                            </div>
                        </Col>
                    </Row>
                        <Row>
                            <Col>
                                <Footer/>
                            </Col>
                        </Row>
                    </div>
                </div>
            
        </BrowserRouter>
        )
    }
}
export default Main;