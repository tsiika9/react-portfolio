import React, { Component } from 'react';
import 'font-awesome/css/font-awesome.min.css';
import { Card, Button, CardTitle, CardText, Container, Row, Col } from 'reactstrap';

class WP extends Component {
    render() {
        return (
            <div>
                <Container>
                    <Row>
                        <Col>
                            <h1>WORDPRESS</h1><br/>
                        </Col>
                    </Row>

                    <Row>
                        <Col md="6">
                            <Card body>
                                <CardTitle>Special Title Treatment</CardTitle>
                                <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                                <Button>Go somewhere</Button>
                            </Card>
                        </Col>
                        
                        <br/>

                        <Col md="6">
                        <Card body>
                                <CardTitle>Special Title Treatment</CardTitle>
                                <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                                <Button>Go somewhere</Button>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}

export default WP;
