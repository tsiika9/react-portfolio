import React, { Component } from 'react';
import 'font-awesome/css/font-awesome.min.css';
import { Container, Row, Col } from 'reactstrap';

class WWorks extends Component {
    render() {
        return (
            <div>
                <Container>
                    <Row>
                        <Col>
                            <h1>Works</h1>
                            <p>At the moment I'm building this page so there is no works on here yet!</p>
                            <p>In the meantime you can check my <a href="https://github.com/tsiika">Github</a> page for my works on there!</p>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}

export default WWorks;
