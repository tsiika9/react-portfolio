import React, { Component } from 'react';
import 'font-awesome/css/font-awesome.min.css';
import { Card, Button, CardTitle, CardText, Container, Row, Col } from 'reactstrap';

class VueJS extends Component {
    render() {
        return (
            <Container>
            <Row>
                <Col>
                    <h1>VUE.JS</h1><br/>
                </Col>
            </Row>

            <Row>
                <Col md="6">
                    <Card body>
                        <CardTitle>Special Title Treatment</CardTitle>
                        <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                        <Button>Go somewhere</Button>
                    </Card>
                </Col>
                
                <br/>

                <Col md="6">
                <Card body>
                        <CardTitle>Special Title Treatment</CardTitle>
                        <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                        <Button>Go somewhere</Button>
                    </Card>
                </Col>
            </Row>
        </Container>
        )
    }
}

export default VueJS;
