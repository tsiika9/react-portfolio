import React, { Component } from 'react';
import { Container, Row, Col, Button, Progress,  } from 'reactstrap';
import './App.css';
import Footer from './modules/Footer';
import NavBar from './modules/Nav';
import tsiika from './img/cv.jpg';
import Projects from './modules/Projects';
import Tech from './modules/Technologies';
import Contact from './modules/Contact';


class App extends Component {
  render() {
    return (
      <div className="App">
          <Container fluid>
            <NavBar/>
            <Row>
              <Col>
                <div className="container-fluid">
                <h1 className="hdr">Tsiika.me</h1>
                  <hr/><br/>
                </div>
              </Col>
            </Row>

            <Row>
              <Col col="xl-9">
                <div className="container ">
                  <h1 id="about">Hello there!</h1>

                  <p className="lead">You have stumbled upon portfolio of Tommi Siik.
                  I'm Finnish web developer currently located at Äänekoski.
                  </p><br/>
                  
                  <p className="lead">
                  I'm currently occupied at Opiframe Oy's full-stack developer program, and there I've been building app for disc golfing with team of other developers.
                  </p>

                  <p className="lead">
                  You can check out the Disc golf project and my other works at my <a href="https://github.com/tsiika">Github page</a>!
                  </p>

                  <br/><hr/><br/>
                  
                  <p className="lead">
                  With my spare time I dedicate that time for other creative hobbies including <a href="https://www.flickr.com/photos/tsiika">photography</a> and music making. 
                  </p>

                  <p className="lead">
                  I also have an radio amateur operator license. 
                  </p>

              </div>
            </Col>

              <Col md="6">
                  <img src={tsiika} alt="Tommi Siik" className="img-fluid" />
              </Col>
            </Row><br/>

          <Row className="bk1">
              <Projects/>
          </Row>

          <Row>
              <Col>
                <Tech/>
              </Col>
          </Row><br/><br/>

            <Row className="bk1">
              <Col>
                <Contact/>
              </Col>
            </Row>

          </Container>
        </div>
    );
  }
}

export default App;
