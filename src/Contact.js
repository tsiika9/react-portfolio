import React, { Component } from 'react';

class Contact extends Component {
    render() {
        return (
            <div>
                <h2>Contact</h2>
                <p>Be brave. Every single thing in the world has its own personality - and it is up to you to make friends with the little rascals. I guess that would be considered a UFO. A big cotton ball in the sky. Maybe there was an old trapper that lived out here and maybe one day he went to check his beaver traps, and maybe he fell into the river and drowned. Painting should do one thing. It should put happiness in your heart. We don't really know where this goes - and I'm not sure we really care.</p>
            </div>
        )
    }
}
export default Contact;
