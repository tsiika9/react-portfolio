import React, { Component } from 'react';
import 'font-awesome/css/font-awesome.min.css';
import '../App.css';


class App extends Component {
render() {
    return (
        <div>
            <h6>Contact</h6>
            
            <p alt="Email" className="lead" style={{ textDecoration: 'none', fontWeight: "500" }}>tsiika9@gmail.com</p>
            <i className="fa fa-linkedin"><a href="https://www.linkedin.com/in/tommi-siik-824473122/" alt="Linkedin"> LinkedIn </a></i>
            <i className="fa fa-github"><a href="https://github.com/tsiika" alt="Github"> Github </a></i>
        </div>
    );
    }
}

export default App;
