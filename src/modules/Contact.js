import React, { Component } from 'react';
import { Row, Col, Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Tooltip } from 'reactstrap';
import 'font-awesome/css/font-awesome.min.css';
import '../App.css';


class Contact extends Component {
    constructor(props) {
        super(props);
    
        this.toggle = this.toggle.bind(this);
        this.state = {
            tooltipOpen: false
        };
    }
    
    toggle() {
        this.setState({
            tooltipOpen: !this.state.tooltipOpen
        });
    }

render() {
    return (
        <div className="container">
        <Row className="ctf">
        <Col md="6">
        <h1 className="display-4" id="contact" style={{color: "#FFFFFF"}}>Contact</h1>
            <p className="lead" style={{color: "#FFFFFF"}}>
            You can contact me via..
            </p>

            <ul className="lead">
            <li><i class="fa fa-linkedin fabio" style={{color: "#FFFFFF"}}></i><a href="https://www.linkedin.com/in/tommi-siik-824473122/" style={{color: "#FFFFFF"}}>LinkedIn</a></li>
            <li><i class="fa fa-github fabio" style={{color: "#FFFFFF"}}></i><a href="https://github.com/tsiika" style={{color: "#FFFFFF"}}>Github</a></li>
            </ul>
        </Col>

        <Col md="6">
            <p className="lead" style={{color: "#FFFFFF"}}>
            Or write me an email!
            </p>
            <Form>
                <FormGroup>
                    <Label for="name" style={{color: "#FFFFFF"}}>Name</Label>
                    <Input type="email" name="name" id="FormEmail" placeholder="Your name" />
                </FormGroup>
                <FormGroup>
                    <Label for="email" style={{color: "#FFFFFF"}}>Email</Label>
                    <Input type="email" name="email" id="FormEmail" placeholder="Email address" />
                </FormGroup>
                <FormGroup>
                    <Label for="text" style={{color: "#FFFFFF"}}>Your message</Label>
                    <Input type="textarea" name="text" id="text" />
                </FormGroup>
                <Button id="tooltip" disabled>Send</Button>
                <Tooltip placement="right" isOpen={this.state.tooltipOpen} target="tooltip" toggle={this.toggle}>
                    Service not available.
                </Tooltip>
            </Form>
        </Col>
        </Row>
        </div>
    );
    }
}

export default Contact;
