import React, { Component } from 'react';
import { Row, Col, Button, CardImg, Card, CardBody, CardTitle, CardSubtitle, CardText, CardFooter } from 'reactstrap';
import Discord from '../img/Discord-Logo.png';
import Necom from '../img/necom.png'

class Projects extends Component {
    render() {
        return (
            <div className="container ctf text-center">
                <h1 className="display-4" id="projects" style={{color: "#FFFFFF"}}>Projects</h1>
            <p className="lead" style={{color: "#FFFFFF"}}>
                Everyone has projects don't they? Down below is few of my personal 
                projects and projects which I have contributed!
            </p>
            
            <div class="card-deck" >
            <Row>
                <Card>
                    <CardBody className="text-center">
                        <CardTitle style={{fontSize: '25px'}}>Disc Golf App</CardTitle>
                        <CardSubtitle className="subt">Opiframe Oy course group project</CardSubtitle><hr/>
                        <CardText className="ct">This is the project I mentioned earlier. Note that the project is split to two parts!</CardText><br/>
                        <CardFooter style={{backgroundColor: '#FFF'}}>
                            <Button href="https://github.com/tsiika/DiscGolf-FrontEnd" style={{margin: '5px', backgroundColor: '#507dbc'}} >Front-end repo</Button>
                            <Button href="https://github.com/svihila/DiscGolf-API" style={{margin: '5px', backgroundColor: '#507dbc'}} >API repo</Button>
                        </CardFooter>
                    </CardBody>
                </Card>

                <Card>
                    <CardBody className="text-center">
                        <CardTitle style={{fontSize: '25px'}}>Jalmari Bot</CardTitle>
                        <CardSubtitle className="subt">Simple bot for Discord messaging software</CardSubtitle><hr/>
                        <CardText className="ct">
                        This project is made for fun when we switched our messaging platform with my friends.
                        Bot can be deployed with Docker.
                        </CardText>
                        <CardFooter style={{backgroundColor: '#FFF'}}>
                            <Button href="https://github.com/tsiika/jalmari-bot" style={{margin: '5px', backgroundColor: '#507dbc'}}>Github</Button>
                        </CardFooter>
                    </CardBody>
                </Card>

                <Card>
                    <CardBody className="text-center">
                        <CardTitle style={{fontSize: '25px'}}>Necom Oy</CardTitle>
                        <CardSubtitle className="subt">Rebuilded web site for Necom Oy</CardSubtitle><hr/>
                        <CardText className="ct">
                            This site was one of my main tasks when I was working at Screenmax Oy.
                            Main technology behind this site is Wordpress.
                        </CardText>
                        <CardFooter style={{backgroundColor: '#FFF'}}>
                            <Button href="http://www.necom.fi/" style={{margin: '5px', backgroundColor: '#507dbc'}}>Necom</Button>
                        </CardFooter>
                    </CardBody>
                </Card>

                <Card>
                    <CardBody className="text-center">
                        <CardTitle style={{fontSize: '25px'}}>dib</CardTitle>
                        <CardSubtitle className="subt">Somewhat new era of the front pages</CardSubtitle><hr/>
                        <CardText className="ct">
                            This project has been in my mind for long time and now it's time to build it!<br/>
                            <u>WIP project</u> and you can see the progress at Github!
                        </CardText>
                        <CardFooter style={{backgroundColor: '#FFF'}}>
                            <Button href="https://github.com/tsiika/dib" style={{margin: '5px', backgroundColor: '#507dbc'}}>Github</Button>
                        </CardFooter>
                    </CardBody>
                </Card>


                </Row>
            </div>
        </div>
        )
    }
}


export default Projects;