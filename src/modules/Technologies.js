import React, { Component } from 'react';
import { Container, Row, Col, Button, Progress,  } from 'reactstrap';
import 'font-awesome/css/font-awesome.min.css';
import '../App.css';


class Tech extends Component {
render() {
    return (
        <div className="container-fluid brk">

            <div className="text-center">
                <h1 className="display-4" id="skills">Skills</h1>
                <p className="lead">
                Here is list of my skills regarding web development:
                </p><br/>
            </div>
        <Row>
            <Col xs="4" md="2" className="ski text-center">
                {/*HTML5*/}
                <i class="devicon-html5-plain-wordmark"></i>
            </Col>
            <Col xs="4" md="2" className="ski text-center">
                {/*CSS3*/}
                <i class="devicon-css3-plain-wordmark"></i>
            </Col>
            <Col xs="4" md="2" className="ski text-center">
                {/*JS*/}
                <i class="devicon-javascript-plain"></i>
            </Col>


            <Col xs="4" md="2" className="ski text-center">
                {/*Node*/}
                <i class="devicon-nodejs-plain-wordmark"></i>
            </Col>
            <Col xs="4" md="2" className="ski text-center">
                {/*React*/}
                <i class="devicon-react-original-wordmark"></i>
            </Col>
            <Col xs="4" md="2" className="ski text-center">
                {/*Angular*/}
                <i class="devicon-angularjs-plain"></i>
            </Col>
        </Row>

        <Row>
            <Col xs="4" md="2" className="ski text-center">
                {/*WP*/}
                <i class="devicon-wordpress-plain-wordmark"></i>
            </Col>
            <Col xs="4" md="2" className="ski text-center">
                {/*Bootstrap*/}
                <i class="devicon-bootstrap-plain-wordmark"></i>
            </Col>
            <Col xs="4" md="2" className="ski text-center">
                {/*Photoshop*/}
                <i class="devicon-photoshop-line"></i>
            </Col>

            <Col xs="4" md="2" className="ski text-center">
                {/*MySQL*/}
                <i class="devicon-mysql-plain-wordmark"></i>
            </Col>

            <Col xs="4" md="2" className="ski text-center">
                {/*Mongo*/}
                <i class="devicon-mongodb-plain-wordmark"></i>
            </Col>
            <Col xs="4" md="2" className="ski text-center">
                {/*Postgre*/}
                <i class="devicon-postgresql-plain-wordmark"></i>
            </Col>

        </Row>
        <Row>
            <Col xs="4" md="2" className="ski text-center">
                {/*Debian*/}
                <i class="devicon-debian-plain-wordmark"></i>
            </Col>
            <Col xs="4" md="2" className="ski text-center">
                {/*Apache*/}
                <i class="devicon-apache-line-wordmark"></i>
            </Col>
            <Col xs="4" md="2" className="ski text-center">
                {/*GIT*/}
                <i class="devicon-git-plain-wordmark"></i>
            </Col>
            <Col xs="4" md="2" className="ski text-center">
                {/*Docker*/}
                <i class="devicon-docker-plain-wordmark"></i>
            </Col>
            <Col xs="4" md="2" className="ski text-center">
                {/*Gulp*/}
                <i class="devicon-gulp-plain"></i>
            </Col>
            <Col xs="4" md="2" className="ski text-center">
                {/*Heroku*/}
                <i class="devicon-heroku-original-wordmark"></i>
            </Col>
        </Row>


        </div>
    );
    }
}

export default Tech;
