import React, { Component } from 'react';
import { Route, BrowserRouter, Link } from 'react-router-dom';
import { Nav, NavItem, NavLink, } from 'reactstrap';

import WWorks from './Works/Index'
import WP from './Works/WP';
import WReact from './Works/React';
import VueJS from './Works/Vue';
import Angular5 from './Works/Angular';



class Works extends Component {


    render() {
        return (
            <BrowserRouter>
            <div>
                    <Nav>
                        <NavItem classID="nav"> <NavLink tag={Link} to="/works">Works</NavLink> </NavItem>
                        <NavItem> <NavLink tag={Link} classID="nav" to="/wp">Wordpress</NavLink> </NavItem>
                        <NavItem> <NavLink tag={Link} classID="nav" to="/react">React</NavLink> </NavItem>
                        <NavItem> <NavLink tag={Link} classID="nav" to="/vuejs">VueJS</NavLink> </NavItem>
                        <NavItem> <NavLink tag={Link} classID="nav" to="/angular5">Angular 5</NavLink> </NavItem>
                    </Nav>     
                    <Route exact path="/works" component={WWorks}/>
                    <Route path="/wp" component={WP}/>
                    <Route path="/react" component={WReact}/>
                    <Route path="/vuejs" component={VueJS}/>
                    <Route path="/angular5" component={Angular5}/>
                

            </div>
            </BrowserRouter>
        )
    }
}
export default Works;
